/*
 * Copyright (c) 2004 flopcode. All Rights Reserved.
 */
package flopcode.pixelmeter;

import javax.swing.*;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial") class ChangeSizeAction extends AbstractAction {
  private final MeterComponent fMeterComponent;
  private final int fDelta;

  public ChangeSizeAction(MeterComponent meterComponent, int delta) {
    fMeterComponent = meterComponent;
    fDelta = delta;
  }

  public void actionPerformed(ActionEvent e) {
    fMeterComponent.setRuleSize(fMeterComponent.getRuleSize() + fDelta);
  }
}
