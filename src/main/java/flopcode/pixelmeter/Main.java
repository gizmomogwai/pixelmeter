package flopcode.pixelmeter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class Main {
  private Properties fSettings;

  private File getSettingsFile() {
    return new File(System.getProperty("user.home") + "/.config/pixelmeter/settings.properties");
  }

  public Main() {
    final JFrame window = new JFrame();
    window.setUndecorated(true);
    window.setBackground(Color.WHITE);

    window.getContentPane().setLayout(new BorderLayout());
    loadSettings();
    window.setLocation(Integer.parseInt(fSettings.getProperty("x", "0")),
        Integer.parseInt(fSettings.getProperty("y", "0")));

    final MeterComponent meterComponent = new MeterComponent(window, fSettings);
    window.getContentPane().add(meterComponent, BorderLayout.CENTER);
    window.addWindowListener(new WindowAdapter() {
      public void windowClosed(WindowEvent event) {
        store(meterComponent, window);
      }
    });

    window.pack();
    window.setVisible(true);
  }

  private void store(MeterComponent meterComponent, JFrame window) {
    meterComponent.store(fSettings);
    fSettings.setProperty("x", "" + window.getX());
    fSettings.setProperty("y", "" + window.getY());
    try {
      File settingsFile = getSettingsFile();
      settingsFile.getParentFile().mkdirs();
      fSettings.store(new FileOutputStream(settingsFile), "pixelmeter settings");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void loadSettings() {
    fSettings = new Properties();
    try {
      fSettings.load(new FileInputStream(getSettingsFile()));
    } catch (IOException e) {
      System.err.println("Cannot load setting from " + getSettingsFile());
    }
  }

  public static void main(String[] args) {
    new Main();
  }

}
