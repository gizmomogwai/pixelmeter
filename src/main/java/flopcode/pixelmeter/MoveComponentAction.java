/*
 * Copyright (c) 2004 flopcode. All Rights Reserved.
 */
package flopcode.pixelmeter;

import java.awt.*;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class MoveComponentAction extends ComponentAction {
  private final int fDx;
  private final int fDy;

  public MoveComponentAction(Component component, int dx, int dy) {
    super(component);
    fDx = dx;
    fDy = dy;
  }


  public void actionPerformed(ActionEvent e) {
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    int x = calcNew(fComponent.getX(), fDx, (int) (screenSize.getWidth()-fComponent.getWidth()));
    int y = calcNew(fComponent.getY(), fDy, (int) (screenSize.getHeight()-fComponent.getHeight()));
    fComponent.setLocation(x, y);
  }

  private int calcNew(int original, int delta, int max) {
    int newRes = original + delta;
    if (newRes < 0) {
      newRes = 0;
    } else if (newRes > max) {
      newRes = max;
    }
    return newRes;
  }
  
}
