/*
 * Copyright (c) 2004 flopcode. All Rights Reserved.
 */
package flopcode.pixelmeter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.geom.Rectangle2D;
import java.util.Properties;

@SuppressWarnings("serial")
public class MeterComponent extends JComponent {
  private static final String HORIZONTAL = "horizontal";
  private static final String VERTICAL = "vertical";

  private static final String LTR = "ltr";
  private static final String RTL = "rtl";

  private static final int WIDTH = 40;

  private int fSize;

  private JFrame fParent;

  private String fOrientiation;

  private String fLeftToRight;
  private Font fBoldFont;
  private Font fNormalFont;

  @SuppressWarnings("serial")
  public MeterComponent(JFrame parent, Properties settings) {
    fParent = parent;
    setBackground(Color.WHITE);
    fSize = Integer.parseInt(settings.getProperty("size", "10"));
    setOrientation(settings.getProperty("orientation", VERTICAL));
    setLeftToRight(settings.getProperty("ltr", LTR));

    createMoveAction(parent, KeyEvent.VK_LEFT, "left", -1, 0);
    createMoveAction(parent, KeyEvent.VK_RIGHT, "right", 1, 0);
    createMoveAction(parent, KeyEvent.VK_UP, "up", 0, -1);
    createMoveAction(parent, KeyEvent.VK_DOWN, "down", 0, 1);
    getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_UP, 0), "bigger");
    getActionMap().put("bigger", new ChangeSizeAction(this, 1));
    getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_UP, KeyEvent.SHIFT_MASK), "fastbigger");
    getActionMap().put("fastbigger", new ChangeSizeAction(this, 10));
    getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_DOWN, 0), "smaller");
    getActionMap().put("smaller", new ChangeSizeAction(this, -1));
    getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_DOWN, KeyEvent.SHIFT_MASK), "fastsmaller");
    getActionMap().put("fastsmaller", new ChangeSizeAction(this, -10));

    getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "escape");
    getActionMap().put("escape", new EscapeAction(fParent));

    getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_F, 0), "flip");
    getActionMap().put("flip", new FlipAction(this));

    getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_H, 0), "horizontal");
    getActionMap().put("horizontal", new OrientationAction(this, "horizontal"));

    getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_V, 0), "vertical");
    getActionMap().put("vertical", new OrientationAction(this, "vertical"));

    getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_V, KeyEvent.SHIFT_MASK), "fullvertical");
    getActionMap().put("fullvertical", new AbstractAction() {
      public void actionPerformed(ActionEvent e) {
        setOrientation(VERTICAL);
        setRuleSize(getToolkit().getScreenSize().height);
        setRuleLocation(getRuleLocation().x, 0);
      }

    });

    getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_H, KeyEvent.SHIFT_MASK), "fullhorizontal");
    getActionMap().put("fullhorizontal", new AbstractAction() {
      public void actionPerformed(ActionEvent e) {
        setOrientation(HORIZONTAL);
        setRuleSize(getToolkit().getScreenSize().width);
        setRuleLocation(0, getRuleLocation().y);
      }
    });

    getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_0, 0), "help");
    getActionMap().put("help", new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        for (KeyStroke keyStroke : getInputMap().allKeys()) {
          Object o = getInputMap().get(keyStroke);
          System.out.println("MeterComponent.actionPerformed: " + keyStroke + " -> " + o);
        }
      }
    });
  }

  public Point getRuleLocation() {
    return fParent.getLocation();
  }

  public void setRuleSize(int size) {
    fSize = size;
    forceReall();
  }

  public void setRuleLocation(int x, int y) {
    fParent.setLocation(x, y);
  }

  public int getRuleSize() {
    return fSize;
  }

  private void setLeftToRight(String property) {
    fLeftToRight = property;
    forceReall();
  }

  private void forceReall() {
    repaint();
    invalidate();
    fParent.invalidate();
    fParent.pack();
  }


  private boolean isLtr() {
    return LTR.equals(fLeftToRight);
  }

  public void flipLtr() {
    if (isLtr()) {
      setLeftToRight(RTL);
    } else {
      setLeftToRight(LTR);
    }
  }

  public void setOrientation(String orientation) {
    fOrientiation = orientation;
    forceReall();
  }

  public void store(Properties settings) {
    settings.setProperty("orientation", fOrientiation);
    settings.setProperty("size", "" + fSize);
    settings.setProperty("ltr", fLeftToRight);
  }

  public Dimension getMinimumSize() {
    if (isVertical()) {
      return new Dimension(WIDTH, fSize);
    } else {
      return new Dimension(fSize, WIDTH);
    }
  }

  public Dimension getPreferredSize() {
    return getMinimumSize();
  }

  private boolean isVertical() {
    return VERTICAL.equals(fOrientiation);
  }

  private void createMoveAction(Component c, int key, String name, int dx, int dy) {
    getInputMap().put(KeyStroke.getKeyStroke(key, 0), name);
    getActionMap().put(name, new MoveComponentAction(c, dx, dy));

    String fastName = "fast" + name;
    getInputMap().put(KeyStroke.getKeyStroke(key, KeyEvent.SHIFT_MASK), fastName);
    getActionMap().put(fastName, new MoveComponentAction(c, dx * 10, dy * 10));
  }

  protected void paintComponent(Graphics g) {
    g.clearRect(0, 0, getWidth(), getHeight());

    getNormalFont(g);
    getBoldFont(g);

    g.setColor(Color.BLACK);
    if (isVertical()) {
      paintVerticalComponent(g);
    } else {
      paintHorizontalComponent(g);
    }
  }

  private void paintHorizontalComponent(Graphics g) {
    Dimension d = getSize();
    if (isLtr()) {
      for (int i = 0; i < d.getWidth(); i += 2) {
        drawHorizontal(i, i, g, d);
      }
    } else {
      for (int i = 0; i < d.getWidth(); i += 2) {
        drawHorizontal((int) (d.getWidth() - i), i, g, d);
      }
    }
  }

  private void drawHorizontal(int xPos, int pos, Graphics g, Dimension d) {
    if ((pos % 100) == 0) {
      g.drawLine(xPos, 0, xPos, (int) d.getHeight());
      g.setFont(getBoldFont(g));
      drawStringCentered(g, xPos, WIDTH / 2, "" + pos);
      g.setFont(getNormalFont(g));
    } else if ((pos % 20) == 0) {
      g.drawLine(xPos, 0, xPos, 10);
      g.setColor(Color.GRAY);
      drawStringCentered(g, xPos, WIDTH / 2, "" + (pos % 100));
      g.setColor(Color.BLACK);
      g.drawLine(xPos, (int) d.getHeight() - 11, xPos, (int) d.getHeight() - 1);
    } else if ((pos % 10) == 0) {
      g.drawLine(xPos, 0, xPos, 6);
      g.drawLine(xPos, (int) d.getHeight() - 7, xPos, (int) d.getHeight() - 1);
    } else {
      g.drawLine(xPos, 0, xPos, 5);
      g.drawLine(xPos, (int) d.getHeight() - 6, xPos, (int) d.getHeight() - 1);
    }
  }

  private void paintVerticalComponent(Graphics g) {
    Dimension d = getSize();
    if (isLtr()) {
      for (int j = 0; j < d.getHeight(); j += 2) {
        drawVertical(j, j, g, d);
      }
    } else {
      for (int j = 0; j < d.getHeight(); j += 2) {
        drawVertical((int) (d.getHeight() - j), j, g, d);
      }
    }

  }

  private void drawVertical(int yPos, int pos, Graphics g, Dimension d) {
    if ((pos % 100) == 0) {
      g.drawLine(0, yPos, WIDTH - 1, yPos);
      g.setFont(getBoldFont(g));
      drawStringCentered(g, WIDTH / 2, yPos, "" + pos);
      g.setFont(getNormalFont(g));
    } else if ((pos % 20) == 0) {
      g.drawLine(0, yPos, 10, yPos);
      g.setColor(Color.GRAY);
      drawStringCentered(g, WIDTH / 2, yPos, "" + (pos % 100));
      g.setColor(Color.BLACK);
      g.drawLine(d.width - 11, yPos, d.width - 1, yPos);
    } else if ((pos % 10) == 0) {
      g.drawLine(0, yPos, 6, yPos);
      g.drawLine(d.width - 7, yPos, d.width - 1, yPos);
    } else {
      g.drawLine(0, yPos, 5, yPos);
      g.drawLine(d.width - 6, yPos, d.width - 1, yPos);
    }
  }

  private Font getBoldFont(Graphics g) {
    if (fBoldFont == null) {
      Font f = g.getFont();
      fBoldFont = f.deriveFont(Font.BOLD);
    }
    return fBoldFont;
  }

  private Font getNormalFont(Graphics g) {
    if (fNormalFont == null) {
      fNormalFont = g.getFont();
    }
    return fNormalFont;
  }

  private void drawStringCentered(Graphics g, int x, int y, String s) {
    FontMetrics fontMetrics = g.getFontMetrics();
    Rectangle2D help = fontMetrics.getStringBounds(s, g);
    g.drawString(s, (int) (x - help.getWidth() / 2), y + fontMetrics.getAscent() / 2);
  }

}
