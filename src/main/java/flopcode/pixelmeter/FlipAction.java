/*
 * Copyright (c) 2004 flopcode. All Rights Reserved.
 */
package flopcode.pixelmeter;

import javax.swing.*;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class FlipAction extends AbstractAction {
  private final MeterComponent fMeterComponent;

  public FlipAction(MeterComponent meterComponent) {
    fMeterComponent = meterComponent;
  }

  public void actionPerformed(ActionEvent e) {
    fMeterComponent.flipLtr();
  }

}
