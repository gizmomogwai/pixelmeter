/*
 * Copyright (c) 2004 flopcode. All Rights Reserved.
 */
package flopcode.pixelmeter;

import java.awt.Component;

import javax.swing.AbstractAction;

public abstract class ComponentAction extends AbstractAction {
  Component fComponent;

  protected ComponentAction(Component component) {
    fComponent = component;
  }

}
