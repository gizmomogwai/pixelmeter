/*
 * Copyright (c) 2004 flopcode. All Rights Reserved.
 */
package flopcode.pixelmeter;

import javax.swing.*;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial") class OrientationAction extends AbstractAction {
  private final MeterComponent fMeterComponent;
  private final String fOrientation;

  public OrientationAction(MeterComponent meterComponent, String orientation) {
    fMeterComponent = meterComponent;
    fOrientation = orientation;
  }

  public void actionPerformed(ActionEvent e) {
    fMeterComponent.setOrientation(fOrientation);
  }
}
