/*
 * Copyright (c) 2004 flopcode. All Rights Reserved.
 */
package flopcode.pixelmeter;

import javax.swing.*;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class EscapeAction extends AbstractAction {
  private final JFrame fParent;

  public EscapeAction(JFrame parent) {
    fParent = parent;
  }

  public void actionPerformed(ActionEvent e) {
    fParent.dispose();
  }
}
