plugins {
  application
}

repositories {
    mavenCentral()
}

application {
  mainClass = "flopcode.pixelmeter.Main"
}

tasks.jar {
    manifest {
        attributes["Main-Class"] = application.mainClass
    }
}
